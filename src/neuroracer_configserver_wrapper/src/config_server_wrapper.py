#!/usr/bin/env python

"""
This file opens a config_server node (a ROS-Node) by running a configserver.
"""

import sys

from neuroracer_common.run_wrapper import run_configserver


def main():
    run_configserver(sys.argv)


if __name__ == "__main__":
    main()
