#!/bin/bash
origin=$PWD
# Requirements
# (clone required repositories)
cd src
git clone https://github.com/ros-drivers/ackermann_msgs.git
git clone https://github.com/mit-racecar/racecar.git
git clone https://github.com/mit-racecar/racecar-simulator.git
git clone https://github.com/wjwwood/serial.git
git clone https://github.com/mit-racecar/vesc.git
cd $origin

# Patches
patched_files=()

## Deprecation warning due to use of 'EffortJointInterface' instead of 'hardware_interface/EffortJointInterface'
patch_macros_xacro=$origin/src/racecar-simulator/racecar_description/urdf/macros.xacro
sed -i 's/EffortJointInterface/hardware_interface\/EffortJointInterface/g' "$patch_macros_xacro"
patched_files+=("$patch_macros_xacro")

## Summary
if [ -n "$patched_files" ]; then
  echo -e "\nPatched files:"
  for i in $patched_files; do
              echo "$i"
          done
fi

# Extenstions
added_files=()

## Launch files
launch_sim=$origin/src/racecar-simulator/racecar_gazebo/launch/neuroracer_sim.launch
cp $origin/extensions/neuroracer_sim.launch "$launch_sim"
added_files+=("$launch_sim")

## Depth Camera
depth_cam_racecar_gazebo=$origin/src/racecar-simulator/racecar_description/urdf/racecar.gazebo
depth_cam_racecar_xacro=$origin/src/racecar-simulator/racecar_description/urdf/racecar.xacro
cp $origin/extensions/racecar.gazebo "$depth_cam_racecar_gazebo"
cp $origin/extensions/racecar.xacro "$depth_cam_racecar_xacro"
added_files+=("$depth_cam_racecar_gazebo")
added_files+=("$depth_cam_racecar_xacro")

## Rotating Lidar 
# (real rotation with time delay of collected data)
# TODO

## Summary
if [ -n "$added_files" ]; then
  echo -e "\nAdded files:"
  for i in $added_files; do
              echo "$i"
          done
fi
