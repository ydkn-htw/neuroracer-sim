# Simulation
TODO

## On this page
- [Requirements](#requirements)
- [Setup](#setup)
- [Architecture](#architecture)
- [Manual Control](#manual-control)
- [MIT-Racecar](#mit-racecar)
- [Imitation Learning](#imitation-learning-sl)
- [Reinforcement Learning](#reinforcement-learning-rl)
- [Media](#media)

## Requirements
* __ros-melodic-desktop-full__  
The included desktop-full's linux packages vary on some distributions, a full list of all used packages can be found [here](ros_packages_list.txt).

## Setup
* Clone this repository and execute [`setupWorkspace.sh`](setupWorkspace.sh).

* To use this catkin workspace it is necessary to install [`neuroracer-common`](https://gitlab.com/NeuroRace/neuroracer-common.git) package via pip. Therefor clone and install the package.
```
git clone https://gitlab.com/NeuroRace/neuroracer-common.git
pip install --user -e neuroracer-common
```

* For autonomous tasks, the package [`neuroracer-ai`](https://gitlab.com/NeuroRace/neuroracer-ai.git) is required.
```
git clone https://gitlab.com/NeuroRace/neuroracer-ai.git
pip install --user -e neuroracer-ai
```

* Further, the [`neuroracer-configserver`](https://gitlab.com/NeuroRace/neuroracer-configserver.git) is used for setup and configuration handling. Clone the repository into [`src`](src/) folder and install the client.
```
cd src
git clone https://gitlab.com/NeuroRace/neuroracer-configserver.git
cd neuroracer-configserver
python setup.py install
```

* Build catkin workspace.

## Architecture
![Imitation Learning Architecture](images/neuroracer-sim-architecture.png)

## Manual Control
![alt Default Controller Layout](https://gitlab.com/NeuroRace/neuroracer-robot/raw/master/images/xbox_one_controller_layout.jpg)

## MIT-Racecar
The `neuroracer-sim` is build atop of the MIT-Racecar and simulation environment, which is a project of the Massachusetts Institute of Technology (Cambridge, USA), providing a basic racecar model and environment. 
The external repositories are differently licensed and not maintained by the NeuroRace team. Some parts have been and others will be modified during future development. See the `patches` and `extensions` sections in [`setupWorkspace.sh`](setupWorkspace.sh) for further details. 

* MIT-Racecar main repository: https://github.com/mit-racecar/
* Used external repositories (differently licensed)
  * Ackermann-Msgs: https://github.com/ros-drivers/ackermann_msgs.git
  * Racecar: https://github.com/mit-racecar/racecar.git
  * Racecar-Simulator: https://github.com/mit-racecar/racecar-simulator.git
  * Serial: https://github.com/wjwwood/serial.git
  * VESC: https://github.com/mit-racecar/vesc.git

## Supervised Learning (SL)
TODO

## Reinforcement Learning (RL)
TODO

## Media
### SL - Constant Speed (2 m/s)
![](http://home.htw-berlin.de/~baumapa/ai_track_overview.mp4)
![](http://home.htw-berlin.de/~baumapa/ai_driver_perspective_color.mp4)
